<?php

namespace WameCms\ShopProductAvailability\Models;

use Petaak\Workdays\WorkdaysUtil;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;
use WameCms\Utils\Cache;
use WameCms\Utils\Calendar;


/**
 * Class DeliveryDate
 * https://packagist.org/packages/petaak/workdays
 *
 * @package WameCms\ShopProductAvailability\Models
 */
class DeliveryDate
{
    public function __construct(
        ShopProductAvailabilityRepository $shopProductAvailabilityRepository
    ) {
        $this->shopProductAvailabilityRepository = $shopProductAvailabilityRepository;
    }


    public function getDate($availabilityId)
    {
        $days = $this->getDays($availabilityId);

//        $date = new \DateTime('2020-11-17');
        $date = new \DateTime('now');
        $workdaysUtil = new WorkdaysUtil('SVK');

        // Ked je menej ako 12 hod a je pracovný deň tak dorucenie + 0 dni inak + 1 den
        $days += date('H') <= 12 && $workdaysUtil->isWorkday($date, 'SVK') ? 0 : 1;

        $workdaysUtil->addWorkdays($date, $days, 'SVK');

        if ($date->format('Y-m-d') == date('Y-m-d', strtotime('+1 day'))) {
            return _('Zajtra');
        } else {
            return $date->format('d.m.Y') . ' (' . strtolower(Calendar::getDays()[$date->format('N')]) . ').';
        }
    }


    private function getDays($availabilityId)
    {
        $cache = Cache::create('shop-product');
        $list = $cache->load('availability-days');

        if (!$list) {
            $list = $this->shopProductAvailabilityRepository->findBy([])->fetchPairs('id', 'delivery_days');
            $cache->save('availability-days', $list, [Cache::EXPIRE => '3 hours']);
        }

        if (isset($list[$availabilityId])) return $list[$availabilityId];

        return null;
    }

}
