<?php

namespace WameCms\ShopProductAvailability\Models;

use App\Model\Shop\ShopProductInCategoryRepository;
use App\Model\Shop\ShopProductRepository;
use App\Model\Shop\Store\ShopStoreProductRepository;
use Nette\Database\Context;
use Nette\Database\Table\ActiveRow;
use Tracy\Debugger;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRuleConditionRepository;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRuleRepository;


class MatchingAvailability
{
    /** @var Context */
    private $database;

    /** @var ShopProductRepository */
    private $shopProductRepository;

    /** @var ShopProductAvailability */
    private $shopProductAvailabilityRepository;

    /** @var ShopProductAvailabilityRuleRepository */
    private $shopProductAvailabilityRuleRepository;

    /** @var ShopProductInCategoryRepository */
    private $shopProductInCategoryRepository;

    /** @var ShopStoreProductRepository */
    private $shopStoreProductRepository;

    /** @var array */
    private $categoryList;

    /** @var array */
    private $storeQuantityList;

    /** @var array */
    private $rulesList;

    /** @var string */
    private $lang;


    public function __construct(
        Context $database,
        ShopProductRepository $shopProductRepository,
        ShopProductAvailabilityRepository $shopProductAvailabilityRepository,
        ShopProductAvailabilityRuleRepository $shopProductAvailabilityRuleRepository,
        ShopProductInCategoryRepository $shopProductInCategoryRepository,
        ShopStoreProductRepository $shopStoreProductRepository
    ) {
        $this->database = $database;
        $this->shopProductRepository = $shopProductRepository;
        $this->shopProductAvailabilityRepository = $shopProductAvailabilityRepository;
        $this->shopProductAvailabilityRuleRepository = $shopProductAvailabilityRuleRepository;
        $this->shopProductInCategoryRepository = $shopProductInCategoryRepository;
        $this->shopStoreProductRepository = $shopStoreProductRepository;

        $this->lang = $shopProductRepository->lang;
    }


    /**
     * Change product availability
     *
     * @param int $productId
     */
    public function change($productId)
    {
        $product = $this->shopProductRepository->findOneBy(['product_id' => $productId]);

        $products = [$productId => $productId];

        if ($product['descendant_product'] == 0) {
            $products = array_replace($this->shopProductRepository->getPairs(['descendant_product' => $productId], 'product_id', 'product_id'), $products);
        }

        $product = null;

        foreach ($products as $id) {
            set_time_limit(600);

            $availability = $this->find($id);

            if ($availability != null) {
//                if ($id == 3565737) {
//                    \Tracy\Debugger::log('UPDATE - ' . $availability);
//                }
                $this->shopProductRepository->update(['product_id' => $id], ['availability_id' => $availability]);
            }

            if ($product['descendant_product'] == 0 && $product['product_id'] != $id) {
                $this->shopProductRepository->updateMainProductAvailability($id);
            }
        }

        $products = null;
    }


    /**
     * Find product availability
     *
     * @param int $productId
     *
     * @return null|ActiveRow
     */
    public function find($productId)
    {
        if ($productId instanceof ActiveRow) $productId = $productId->product_id;

        $this->product = $this->shopProductRepository->findOneBy(['product_id' => $productId]);

        $availabilityList = $this->shopProductAvailabilityRepository->getAvailabilityList();
        $categories = $this->shopProductInCategoryRepository->getProductCategoryList($productId);
        $categoryList = isset($categories[$productId]) ? $categories[$productId] : [];
        $this->categoryList = array_column($categoryList, 'category_id');
        $this->rulesList = $this->shopProductAvailabilityRuleRepository->getRules(null, false);

        $return = $this->getAvailability($productId, $availabilityList);

//        if ($productId == 3565737) {
//            \Tracy\Debugger::log('find - RETURN');
//            \Tracy\Debugger::log($return);
//        }

        return $return;
    }


    /**
     * Get availability
     *
     * @param int $productId
     * @param array $availabilityList
     *
     * @return null|ActiveRow
     */
    private function getAvailability($productId, $availabilityList)
    {
        foreach ($this->rulesList as $rule) {
            set_time_limit(600);

            $availability = $availabilityList[$rule['availability_id']];

            if (count($rule['categories']) > 0) {
                $check = $this->checkCategories($availability);

                if ($check == false) continue;
            }

            if (count($rule['conditions']) > 0) {
                $check = $this->checkRules($productId, $rule);

                if ($check) return $availability['id'];
            }
        }

        return null;
    }


    /**
     * Check product in categories
     *
     * @param ActiveRow $availability
     *
     * @return bool
     */
    private function checkCategories($availability)
    {
        if (!isset($this->rulesList[$availability['sort']]['categories'])) return true;

        $list = $this->rulesList[$availability['id']]['categories'];

        foreach ($this->categoryList as $categoryId) {
            if (in_array($categoryId, $list)) {
                $list = null;

                return true;
            }
        }

        $list = null;

        return false;
    }


    /**
     * Check availability rules
     *
     * @param int $productId
     * @param array $rule
     *
     * @return \Nette\Database\ResultSet
     */
    private function checkRules($productId, $rule)
    {
        set_time_limit(600);

        $list = $this->shopStoreProductRepository->findBy(['product_id' => $productId])->select('store_id, quantity, description')->fetchPairs('store_id');

//        \Tracy\Debugger::log('$list - ' . count($list));
//        \Tracy\Debugger::log($list);

        if (count($list) == 0) return false;

        $conditions = $rule['conditions'];

        $results = [];

        foreach ($conditions as $index => $condition) {
            $stock = isset($list[$condition['store_id']]) ? $list[$condition['store_id']]['quantity'] : null;
            if ($stock === null) continue;
//            if ($condition['note'] == ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_HAVE && !$list[$condition['store_id']]['note']) continue;
//            if ($condition['note'] == ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_NOT_HAVE && $list[$condition['store_id']]['note']) continue;

            $results[$index] = $this->checkRuleCondition($condition['expression'], $stock, $condition['value']);
        }

//        \Tracy\Debugger::log('$results - ' . count($results));
//        \Tracy\Debugger::log($results);

        if (count($results) == 0) return false;

//        $conditionsI = 1;

        foreach ($conditions as $index => $condition) {
            if (!isset($results[$index])) continue;

            $result = $results[$index];

//            \Tracy\Debugger::log('$conditionsI - ' . $conditionsI++);

            // Prvé pravidlo ak vrati false všetko ostatné neplatí
            if ($index == 0) {
                if ($result == false) {
                    return false;
                } else {
                    continue;
                }
            }

            if ($condition['condition'] == 'AND' && $result == false) {
                return false;
            } elseif ($condition['condition'] == 'OR') {
                return $this->resultOr($conditions, $index, $results);
            }
        }

        return true;
    }


    private function checkRuleCondition($expression, $stock, $quantity)
    {
        switch ($expression) {
            case '=':
                if ($stock == $quantity) { return true; } else { return false; }
            case '!=':
                if ($stock != $quantity) { return true; } else { return false; }
            case '>=':
                if ($stock >= $quantity) { return true; } else { return false; }
            case '>':
                if ($stock > $quantity) { return true; } else { return false; }
            case '<>':
                if ($stock <> $quantity) { return true; } else { return false; }
            case '<':
                if ($stock < $quantity) { return true; } else { return false; }
            case '<=':
                if ($stock <= $quantity) { return true; } else { return false; }
            default:
                return false;
        }

        return false;
    }


    private function resultOr($conditions, $index, $results)
    {
        foreach ($conditions as $i => $condition) {
            if ($i < $index) continue;

            $result = $results[$index];

            if ($result == true) return true;

            $previous = prev($conditions);

            if ($previous) {
                $previousResult = isset($results[$index - 1]) ? $results[$index - 1] : null;

                if ($previousResult == true) return true;

                if ($previous['condition'] == 'OR') {
                    return $this->resultOr($conditions, $index - 1, $results);
                }
            }

            return false;
        }
    }

}
