<?php

namespace WameCms\ShopProductAvailability\Models;

use App\Model\Shop\ShopCategoryRepository;
use App\Model\Shop\ShopProductRepository;
use App\Model\Shop\Store\ShopStoreRepository;
use Nette\Database\Context;
use Nette\DI\Container;
use Tracy\Debugger;
use WameCms\LoyaltySystem\Models\UpdateProductPriceFactory;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRuleConditionRepository;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRuleRepository;


class Regenerate
{
    /** @var Container */
    private $container;

    /** @var Context */
    private $database;

    /** @var ShopCategoryRepository */
    private $shopCategoryRepository;

    /** @var ShopProductRepository */
    private $shopProductRepository;

    /** @var ShopProductAvailabilityRuleRepository */
    private $shopProductAvailabilityRuleRepository;

    /** @var ShopStoreRepository */
    private $shopStoreRepository;


    public function __construct(
        Container $container,
        Context $database,
        ShopCategoryRepository $shopCategoryRepository,
        ShopProductRepository $shopProductRepository,
        ShopProductAvailabilityRuleRepository $shopProductAvailabilityRuleRepository,
        ShopStoreRepository $shopStoreRepository
    ) {
        $this->container = $container;
        $this->database = $database;
        $this->shopCategoryRepository = $shopCategoryRepository;
        $this->shopProductRepository = $shopProductRepository;
        $this->shopProductAvailabilityRuleRepository = $shopProductAvailabilityRuleRepository;
        $this->shopStoreRepository = $shopStoreRepository;
    }


    public function process()
    {
        // Aktualizuje počty kusov variant zo všetkých skladov
        $this->updateDescendantProductStockStatus();

        // Zmena statusu variant (publikovaný/nepublikovaný)
        $this->shopProductRepository->update(['descendant_product != ?' => 0, 'stock_status <= ? OR eshop = ?' => [0, ShopProductRepository::ESHOP_DISABLED]], ['status' => 5]);
        $this->shopProductRepository->update(['descendant_product != ?' => 0, 'stock_status > ? OR eshop = ?' => [0, ShopProductRepository::ESHOP_ENABLED]], ['status' => 2]);

        // Ak má varianta vypnuté zdieľanie s eshopom skontroluje stav skladu u dodávateľoch a ak nikde nieje na sklade tak vypne produkt
        $this->updateIfEshopNotShowVariant();

        // Aktualizuje počty ksuov hlavných produktov z variant
        $this->updateMainProductStockStatus();

        // Zmena statusu hlavných produktov (publikovaný/nepublikovaný)
        $this->shopProductRepository->update(['descendant_product' => 0, 'stock_status <= ? OR eshop = ?' => [0, ShopProductRepository::ESHOP_DISABLED]], ['status' => 0]);
        $this->shopProductRepository->update(['descendant_product' => 0, 'stock_status > ? OR eshop = ?' => [0, ShopProductRepository::ESHOP_ENABLED]], ['status' => 1]);

        // Ak má produkt vypnuté zdieľanie s eshopom skontroluje stav skladu u dodávateľoch a ak nikde nieje na sklade tak vypne produkt
        $this->updateIfEshopNotShow();

        // Aktualizuje dostupnosť variant
        $this->updateDescendantProductAvailability();

        // Zmena statusu variant ktoré majú stav dostupnosti Plánovaná výroba
        $this->shopProductRepository->update(['descendant_product != ?' => 0, 'availability_id' => 6], ['status' => 2]);

        // Aktualizuje dostupnosť hlavných produktov podľa variant
        $this->updateMainProductAvailability();

        // Zmena statusu hlavných produktov ktoré majú stav dostupnosti Plánovaná výroba
        $this->shopProductRepository->update(['descendant_product' => 0, 'availability_id' => 6], ['status' => 1]);

        // Aktualizovaným produktom vráti import_status na 0
        $this->shopProductRepository->update(['import_status' => '1'], ['import_status' => '0']);

        // Upraví sumu produktov pre vernostný systém
        $this->container->getByType(UpdateProductPriceFactory::class)->create()->updateAll();
    }


    private function updateIfEshopNotShowVariant()
    {
        $query = "UPDATE wame_shop_product AS p "
            . "INNER JOIN (SELECT product_id, SUM(quantity) AS summary FROM wame_shop_store_product WHERE store_id NOT IN (?) GROUP BY product_id) AS sp ON sp.product_id = p.product_id "
            . "SET p.status = ? "
            . "WHERE p.eshop = ? AND p.descendant_product != ? AND sp.summary <= ?";

        $this->database->query($query, [13,14,16], 5, 0, 0, 0);
    }


    private function updateIfEshopNotShow()
    {
        $query = "UPDATE wame_shop_product AS p "
            . "INNER JOIN (SELECT product_id, SUM(quantity) AS summary FROM wame_shop_store_product WHERE store_id NOT IN (?) GROUP BY product_id) AS sp ON sp.product_id = p.product_id "
            . "SET p.status = ? "
            . "WHERE p.eshop = ? AND p.descendant_product = ? AND sp.summary <= ?";

        $this->database->query($query, [13,14,16], 0, 0, 0, 0);
    }


    private function updateDescendantProductStockStatus()
    {
        $query = "UPDATE wame_shop_product AS p "
            . "SET p.stock_status = (SELECT SUM(sp.quantity) FROM wame_shop_store_product AS sp WHERE sp.product_id = p.product_id) "
            . "WHERE p.descendant_product != ?";

        $this->database->query($query, 0);
    }


    private function updateMainProductStockStatus()
    {
        $stores = $this->shopStoreRepository->findBy([]);

        foreach ($stores as $store) {
            $query = "UPDATE wame_shop_store_product AS sp "
                . "LEFT JOIN wame_shop_product AS p ON p.product_id = sp.product_id "
                . "JOIN (SELECT p3.descendant_product FROM wame_shop_product AS p3 GROUP BY p3.descendant_product HAVING COUNT(*) > 0) p4 ON p4.descendant_product = sp.product_id "
                . "SET sp.quantity = (SELECT SUM(sp2.quantity) FROM (SELECT * FROM wame_shop_store_product) AS sp2 LEFT JOIN wame_shop_product AS p2 ON p2.product_id = sp2.product_id WHERE sp2.store_id = ? AND p2.descendant_product = sp.product_id AND p2.status = ?) "
                . "WHERE p.descendant_product = ? AND sp.store_id = ?";
            $this->database->query($query, $store->id, 2, 0, $store->id);
        }

        $query = "UPDATE wame_shop_product AS p "
            . "SET p.stock_status = (SELECT SUM(sp.quantity) FROM wame_shop_store_product AS sp WHERE sp.product_id = p.product_id) "
            . "WHERE p.descendant_product = ?";

        $this->database->query($query, 0);

        $query = "UPDATE wame_shop_product AS p "
            . "JOIN (SELECT p2.descendant_product FROM wame_shop_product AS p2 GROUP BY p2.descendant_product HAVING COUNT(*) > 0) p3 ON p3.descendant_product = p.product_id "
            . "INNER JOIN (SELECT p4.descendant_product, SUM(p4.stock_status) AS quantity FROM wame_shop_product AS p4 WHERE p4.descendant_product != ? AND p4.status = ?) AS p5 ON p5.descendant_product = p.product_id "
            . "SET p.stock_status = p5.quantity "
            . "WHERE p.descendant_product = ?";

        $this->database->query($query, 0, 2, 0);
    }


    private function updateDescendantProductAvailability()
    {
        $list = $this->shopProductAvailabilityRuleRepository->getRules(null, false);

        $categoryList = $this->shopCategoryRepository->getSubcategoryList(null, false);

        foreach ($list as $rule) {
            $categoryIds = [];

            foreach (array_keys($rule['categories']) as $categoryId) {
                $categoryIds[$categoryId] = $categoryId;

                if (isset($categoryList[$categoryId])) {
                    foreach ($categoryList[$categoryId] as $subcategoryId) {
                        $categoryIds[$subcategoryId] = $subcategoryId;
                    }
                }
            }

            $args = [];

            $query = "UPDATE wame_shop_product AS p ";

            foreach ($rule['conditions'] as $key => $condition) {
                $query .= "LEFT JOIN ("
                        . "SELECT supp$key.product_id AS sup" . $key . "productId, supp$key.quantity AS sup" . $key . "quantity, supp$key.description AS sup" . $key . "note "
                        . "FROM wame_shop_store_product AS supp$key "
                        . "WHERE supp$key.store_id = ?"
                        . ") AS sup$key ON sup$key.sup" . $key . "productId = p.product_id ";
                $args[] = $condition['store_id'];
            }

            $query .= "SET p.availability_id = ?, import_status = ? "
                . "WHERE (p.import_status = ? AND p.descendant_product != ?";

            $args[] = $rule['availability_id'];
            $args[] = '1';
            $args[] = '0';
            $args[] = 0;

            if (count($categoryIds) > 0) {
                $query .= " AND EXISTS ("
                        . "SELECT pic0.product_id "
                        . "FROM wame_shop_product_in_category AS pic0 "
                        . "WHERE pic0.category_id IN (?) AND pic0.product_id = p.descendant_product "
                        . "GROUP BY pic0.product_id HAVING COUNT(*) > 0)";
                $args[] = $categoryIds;
            }

            $conditions = '';

            foreach ($rule['conditions'] as $key => $condition) {
                $cond = "(CASE WHEN sup$key.sup" . $key . "quantity IS NULL THEN 0 ELSE sup$key.sup" . $key . "quantity END " . $condition['expression'] . " ?";
                $args[] = $condition['value'];

                if ($condition['note'] == ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_HAVE) {
                    $cond .= " AND CASE WHEN sup$key.sup" . $key . "note IS NULL THEN '' ELSE sup$key.sup" . $key . "note END != ?";
                    $args[] = '';
                } elseif ($condition['note'] == ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_NOT_HAVE) {
                    $cond .= " AND CASE WHEN sup$key.sup" . $key . "note IS NULL THEN '' ELSE sup$key.sup" . $key . "note END = ?";
                    $args[] = '';
                }

                $cond .= ")";

                if ($condition['condition'] == 'OR') {
                    $previous = isset($rule['conditions'][$key - 1]) ? $rule['conditions'][$key - 1] : null;

                    if ($previous) {
                        if ($key - 1 == 0 || $previous['condition'] == 'OR') {
                            $conditions .= " OR " . $cond;
                        } else {
                            $conditions .= ") AND (" . $cond;
                        }
                    } else {
                        $conditions .= ") AND (" . $cond;
                    }
                } else {
                    $conditions .= ") AND (" . $cond;
                }
            }

            if ($conditions != '') $conditions .= ')';

            $this->database->queryArgs($query . $conditions, $args);
        }
    }


    private function updateMainProductAvailability()
    {
        $list = $this->shopProductAvailabilityRuleRepository->getRules(null, false);

        $categoryList = $this->shopCategoryRepository->getSubcategoryList(null, false);

        \Tracy\Debugger::$maxLength = 0;

        foreach ($list as $rule) {
            $categoryIds = [];

            foreach(array_keys($rule['categories']) as $categoryId) {
                $categoryIds[$categoryId] = $categoryId;

                if (isset($categoryList[$categoryId])) {
                    foreach ($categoryList[$categoryId] as $subcategoryId) {
                        $categoryIds[$subcategoryId] = $subcategoryId;
                    }
                }
            }

            $args = [];

            $query = "UPDATE wame_shop_product AS p ";

            foreach ($rule['conditions'] as $key => $condition) {
                $query .= "LEFT JOIN ("
                        . "SELECT supp$key.product_id AS sup" . $key . "productId, supp$key.quantity AS sup" . $key . "quantity, supp$key.description AS sup" . $key . "note "
                        . "FROM wame_shop_store_product AS supp$key "
                        . "WHERE supp$key.store_id = ?"
                        . ") AS sup$key ON sup$key.sup" . $key . "productId = p.product_id ";
                $args[] = $condition['store_id'];
            }

            $query .= "SET p.availability_id = ?, import_status = ? "
                . "WHERE (p.import_status = ? AND p.descendant_product = ?";

            $args[] = $rule['availability_id'];
            $args[] = '1';
            $args[] = '0';
            $args[] = 0;

            if (count($categoryIds) > 0) {
                $query .= " AND EXISTS ("
                        . "SELECT pic0.product_id "
                        . "FROM wame_shop_product_in_category AS pic0 "
                        . "WHERE pic0.category_id IN (?) AND pic0.product_id = p.product_id "
                        . "GROUP BY pic0.product_id HAVING COUNT(*) > 0)";
                $args[] = $categoryIds;
            }

            $conditions = '';

//            dump($rule['conditions']);

            foreach ($rule['conditions'] as $key => $condition) {
                $cond = "(CASE WHEN sup$key.sup" . $key . "quantity IS NULL THEN 0 ELSE sup$key.sup" . $key . "quantity END " . $condition['expression'] . " ?";
                $args[] = $condition['value'];

                if ($condition['note'] == ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_HAVE) {
                    $cond .= " AND CASE WHEN sup$key.sup" . $key . "note IS NULL THEN '' ELSE sup$key.sup" . $key . "note END != ?";
                    $args[] = '';
                } elseif ($condition['note'] == ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_NOT_HAVE) {
                    $cond .= " AND CASE WHEN sup$key.sup" . $key . "note IS NULL THEN '' ELSE sup$key.sup" . $key . "note END = ?";
                    $args[] = '';
                }

                $cond .= ")";

                if ($condition['condition'] == 'OR') {
                    $previous = isset($rule['conditions'][$key - 1]) ? $rule['conditions'][$key - 1] : null;

                    if ($previous) {
                        if ($key - 1 == 0 || $previous['condition'] == 'OR') {
                            $conditions .= " OR " . $cond;
                        } else {
                            $conditions .= ") AND (" . $cond;
                        }
                    } else {
                        $conditions .= ") AND (" . $cond;
                    }
                } else {
                    $conditions .= ") AND (" . $cond;
                }
            }

            if ($conditions != '') $conditions .= ')';

            $this->database->queryArgs($query . $conditions, $args);
        }
    }

}
