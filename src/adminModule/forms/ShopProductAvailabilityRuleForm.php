<?php

namespace WameCms\ShopProductAvailability\AdminModule\Forms;

use App\Model\Shop\ShopCategoryRepository;
use App\Model\Shop\Store\ShopStoreRepository;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;
use Nette\Security\User;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRuleConditionRepository;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRuleRepository;


interface ShopProductAvailabilityRuleFormFactory
{
    /** @return ShopProductAvailabilityRuleForm */
    public function create();
}


class ShopProductAvailabilityRuleForm
{
    /** @var User */
    private $user;

    /** @var ShopCategoryRepository */
    private $shopCategoryRepository;

    /** @var ShopProductAvailabilityRepository */
    private $shopProductAvailabilityRepository;

    /** @var ShopProductAvailabilityRuleRepository */
    private $shopProductAvailabilityRuleRepository;

    /** @var ShopStoreRepository */
    private $shopStoreRepository;

    /** @var ActiveRow */
    private $entity;


    public function __construct(
        User $user,
        ShopCategoryRepository $shopCategoryRepository,
        ShopProductAvailabilityRepository $shopProductAvailabilityRepository,
        ShopProductAvailabilityRuleRepository $shopProductAvailabilityRuleRepository,
        ShopStoreRepository $shopStoreRepository
    ) {
        $this->user = $user;
        $this->shopCategoryRepository = $shopCategoryRepository;
        $this->shopProductAvailabilityRepository = $shopProductAvailabilityRepository;
        $this->shopProductAvailabilityRuleRepository = $shopProductAvailabilityRuleRepository;
        $this->shopStoreRepository = $shopStoreRepository;
    }


    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }


    public function build()
    {
        $form = new Form();

        $form->addSelect('availability_id', _('Dostupnosť'), $this->getAvailabilityList());

        $form->addText('sort', _('Poradie'))
                ->setType('number')
                ->setDefaultValue(1)
                ->setRequired(true);

        $form->addMultiSelect('categories', _('Kategórie'), $this->getCategoryList())
                ->setAttribute('placeholder', _('Vyberte kategórie zo zoznamu. Ak nebude zvolená žiadna tak platí pre všetky.'));

        $multiplier = $form->addMultiplier('conditions', function (\Nette\Forms\Container $container, \Nette\Forms\Form $form)
        {
            $container->addSelect('condition', _('Podmienka'), ['AND', 'OR'])
                        ->setItems(['AND', 'OR'], false);

            $container->addSelect('store_id', _('Sklad'), $this->getStoresList());

            $container->addSelect('expression', _('Výraz'))
                        ->setItems(['=', '!=', '>=', '>', '<>', '<', '<='], false);

            $container->addText('value', _('Hodnota'))
                        ->setType('number')
                        ->setAttribute('placeholder', 0)
                        ->setRequired(true);

            $container->addSelect('note', _('Poznámka'), [ShopProductAvailabilityRuleConditionRepository::NOTE_NONE => _('Nerozhoduje'), ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_HAVE => _('Musí mať poznámku od dodávateľa'), ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_NOT_HAVE => _('Nesmie mať poznámku od dodávateľa')]);
        }, 1, null);

        $multiplier->addCreateButton(_('+ Pridať pravidlo'));
        $multiplier->addRemoveButton(_('Odstrániť'));

        if ($this->entity) {
            $form->addSubmit('submit', _('Upraviť'));

            $form->setDefaults($this->entity);
        } else {
            $form->addSubmit('submit', _('Pridať'));
        }

        $form->onSuccess[] = [$this, 'formSuccess'];

        return $form;
    }


    public function formSuccess(Form $form, $values)
    {
        $presenter = $form->getPresenter();
        $lang = $presenter->lang;

        if ($this->entity) {
            $this->shopProductAvailabilityRuleRepository->edit($this->entity['id'], $values);

            $presenter->flashMessage(_('Pravidlo bolo úspešne upravené.'), 'success');
            $presenter->redirect('this');
        } else {
            $insert = $this->shopProductAvailabilityRuleRepository->create($values);

            if ($insert) {
                $presenter->flashMessage(_('Pravidlo bolo úspešne pridané.'), 'success');
            } else {
                $presenter->flashMessage(_('Nepodarilo sa pridať pravidlo.'), 'danger');
            }

            $presenter->redirect(':Admin:ShopProductsAvailability:', ['id' => null]);
        }
    }


    /**
     * Get availability list
     *
     * @return array
     */
    private function getAvailabilityList()
    {
        $list = $this->shopProductAvailabilityRepository->getAvailabilityList();

        $return = [];

        foreach ($list as $item) {
            $return[$item['id']] = $item['title'];
        }

        return $return;
    }


    /**
     * Get stores list
     *
     * @return array
     */
    private function getStoresList()
    {
        return $this->shopStoreRepository->getPairs(['status != ?' => ShopStoreRepository::STATUS_DELETED], 'id', 'title', 'title ASC');
    }


    /**
     * Get category list
     *
     * @return array
     */
    private function getCategoryList()
    {
        $categories = $this->shopCategoryRepository->getObjectPairs(['status != ?' => 0], 'category_id');
        $allCategories = $this->shopCategoryRepository->getPairs([], 'id', 'title');

        $list = [];

        foreach ($categories as $categoryId => $category) {
            $list[$categoryId] = $category['title'];
        }

        ksort($list);

        $return = [];

        foreach ($list as $catId => $cat) {
            if ($categories[$catId]['parent_category_id']) {
                $return[$catId] = $allCategories[$categories[$catId]['parent_category_id']] . ' > ' . $cat;
            } else {
                $return[$catId] = $cat;
            }
        }

        asort($return);

        return $return;
    }

}
