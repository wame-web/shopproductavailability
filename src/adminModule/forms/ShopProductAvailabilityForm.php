<?php

namespace WameCms\ShopProductAvailability\AdminModule\Forms;

use App\Model\Shop\ShopCategoryRepository;
use App\Model\Shop\Store\ShopStoreRepository;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;
use Nette\Security\User;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;


interface ShopProductAvailabilityFormFactory
{
    /** @return ShopProductAvailabilityForm */
    public function create();
}


class ShopProductAvailabilityForm
{
    /** @var User */
    private $user;

    /** @var ShopCategoryRepository */
    private $shopCategoryRepository;

    /** @var ShopProductAvailabilityRepository */
    private $shopProductAvailabilityRepository;

    /** @var ShopStoreRepository */
    private $shopStoreRepository;

    /** @var ActiveRow */
    private $entity;


    public function __construct(
        User $user,
        ShopCategoryRepository $shopCategoryRepository,
        ShopProductAvailabilityRepository $shopProductAvailabilityRepository,
        ShopStoreRepository $shopStoreRepository
    ) {
        $this->user = $user;
        $this->shopCategoryRepository = $shopCategoryRepository;
        $this->shopProductAvailabilityRepository = $shopProductAvailabilityRepository;
        $this->shopStoreRepository = $shopStoreRepository;
    }


    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }


    public function build()
    {
        $form = new Form();

        $form->addText('title', _('Názov'))
                ->setRequired(true);

        $form->addTextArea('description', _('Popis'));

        $form->addText('delivery_days', _('Doba dodania v dňoch'))
                ->setType('number')
                ->setRequired(true);

        $form->addSelect('delivery_days_heureka', _('Dodacia doba produktu pre Heuréku'))
                ->setPrompt(_('info v obchode'))
                ->setItems([0 => _('skladom'), 1 => _('do 3 dní'), 4 => _('do týždňa'), 8 => _('do 2 týždňov'), 15 => _('do mesiaca'), 31 => _('viac ako mesiac')]);

        $form->addText('sort', _('Poradie'))
                ->setType('number')
                ->setRequired(true);

        $form->addText('icon', _('Ikona'));

        $form->addText('color', _('Farba'))
                ->setType('color')
                ->setAttribute('placeholder', '#000000')
                ->setRequired(true);

        $form->addCheckbox('add_to_cart', _('Je možné pridať do košíka'));

        if ($this->entity) {
            $form->addSubmit('submit', _('Upraviť'));

            $form->setDefaults($this->entity);
        } else {
            $form->addSubmit('submit', _('Pridať'));
        }

        $form->onSuccess[] = [$this, 'formSuccess'];

        return $form;
    }


    public function formSuccess(Form $form, $values)
    {
        $presenter = $form->getPresenter();
        $lang = $presenter->lang;

        if ($this->entity) {
            $this->shopProductAvailabilityRepository->edit($this->entity->id, $lang, $values);

            $presenter->flashMessage(sprintf(_('Dostupnosť %s bola úspešne upravená.'), $values['title']), 'success');
        } else {
            $insert = $this->shopProductAvailabilityRepository->create($lang, $values);

            if ($insert) {
                $presenter->flashMessage(sprintf(_('Dostupnosť %s bola úspešne pridaná.'), $values['title']), 'success');
            } else {
                $presenter->flashMessage(sprintf(_('Nepodarilo sa pridať dostupnosť %s.'), $values['title']), 'danger');
            }
        }

        $presenter->redirect(':Admin:ShopProductsAvailability:', ['id' => null]);
    }

}
