<?php

namespace App\AdminModule\Presenters;

use Nette\Database\Table\ActiveRow;
use WameCms\ShopProductAvailability\AdminModule\Controls\AvailabilityListControlFactory;
use WameCms\ShopProductAvailability\AdminModule\Grids\ShopProductAvailabilityRuleGridFactory;
use WameCms\ShopProductAvailability\AdminModule\Forms\ShopProductAvailabilityFormFactory;
use WameCms\ShopProductAvailability\Models\Regenerate;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;


class ShopProductsAvailabilityPresenter extends AdminPresenter
{
    /** @var ShopProductAvailabilityRepository @inject */
    public $repository;

    /** @var ShopProductAvailabilityFormFactory @inject */
    public $form;

    /** @var ShopProductAvailabilityRuleGridFactory @inject */
    public $grid;

    /** @var AvailabilityListControlFactory @inject */
    public $availabilityListControlFactory;

    /** @var Regenerate @inject */
    public $regenerate;

    /** @var ActiveRow */
    private $entity;


    /** action ********************************************************************************************************/

    public function actionDefault()
    {
        if ($this->id) {
            $this->entity = $this->getEntity(false);
        }

        $this->addComponent($this->grid->create(), 'grid');
        $this->addComponent($this->availabilityListControlFactory->create($this->entity), 'availabilityList');
    }


    public function actionCreate()
    {
        $this->addComponent($this->form->create()->build(), 'form');
    }


    public function actionEdit()
    {
        $this->getEntity();

        $this->addComponent($this->form->create()->setEntity($this->entity)->build(), 'form');
    }


    public function actionDelete()
    {
        $this->getEntity();
    }


    /** handle ********************************************************************************************************/

    public function handleDelete()
    {
        if (is_numeric($this->id)) {
            $this->repository->update(['id' => $this->id], ['edit_date' => date('Y-m-d H:i:s'), 'edit_user_id' => $this->user->id, 'status' => 0]);

            $this->flashMessage(_('Dostupnosť produktu bola úspešne odstránená.'), 'success');
        }

        $this->redirect(':Admin:ShopProductsAvailability:', ['id' => null]);
    }


    public function handleRegenerate()
    {
//        try {
            $this->regenerate->process();

            $this->flashMessage(_('Pregenerovanie prebehlo úspešne, skontrolujte produkty.'), 'success');
//        } catch (\Exception $e) {
//            $this->flashMessage($e->getMessage());
//        }

        $this->redirect('this');
    }


    /** render ********************************************************************************************************/

    public function renderDefault()
    {
        $this->template->siteTitle = _('Dostupnosti produktov');

        $this->template->setFile($this->getTemplatePath() . 'default.latte');
    }


    public function renderCreate()
    {
        $this->template->siteTitle = _('Pridať dostupnosť produktov');

        $this->template->setFile($this->getTemplatePath() . 'edit.latte');
    }


    public function renderEdit()
    {
        $this->template->siteTitle = _('Upraviť dostupnosť produktov');
        $this->template->siteSubTitle = $this->entity->title;

        $this->template->setFile($this->getTemplatePath() . 'edit.latte');
    }


    public function renderDelete()
    {
        $this->template->siteTitle = _('Odstrániť dostupnosť produktov');
        $this->template->siteSubTitle = $this->entity->title;

        $this->template->setFile($this->getTemplatePath() . 'delete.latte');
    }


    /** other *********************************************************************************************************/

    /**
     * Get entity
     *
     * @param bool $check
     *
     * @return ActiveRow
     *
     * @throws \Nette\Application\AbortException
     */
    private function getEntity($check = true)
    {
        if ($this->entity) return $this->entity;

        if (!$this->id && $check === true) {
            $this->flashMessage(_('Nemáte zadané ID dostupnosti'), 'danger');
            $this->redirect(':Admin:ShopProductsAvailability:', ['id' => null]);
        }

        $this->entity = $this->repository->getAvailability($this->id);

        if (!$this->entity) {
            $this->flashMessage(_('Dostupnosť s takýmto ID sa nenašla'), 'danger');
            $this->redirect(':Admin:ShopProductsAvailability:', ['id' => null]);
        }

        return $this->entity;
    }


    /**
     * Get template path
     *
     * @return string
     */
    private function getTemplatePath()
    {
        return __DIR__ . '/templates/ShopProductAvailability/';
    }

}
