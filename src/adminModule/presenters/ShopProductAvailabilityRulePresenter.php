<?php

namespace App\AdminModule\Presenters;

use Nette\Database\Table\ActiveRow;
use WameCms\ShopProductAvailability\AdminModule\Forms\ShopProductAvailabilityRuleFormFactory;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRuleRepository;


class ShopProductAvailabilityRulePresenter extends AdminPresenter
{
    /** @var ShopProductAvailabilityRuleRepository @inject */
    public $repository;

    /** @var ShopProductAvailabilityRuleFormFactory @inject */
    public $form;

    /** @var ActiveRow */
    private $entity;


    /** action ********************************************************************************************************/

    public function actionCreate()
    {
        $this->addComponent($this->form->create()->build(), 'form');
    }


    public function actionEdit()
    {
        $this->getEntity();

        $this->addComponent($this->form->create()->setEntity($this->entity)->build(), 'form');
    }


    public function actionDelete()
    {
        $this->getEntity();
    }


    /** handle ********************************************************************************************************/

    public function handleDelete()
    {
        if (is_numeric($this->id)) {
            $this->repository->delete(['id' => $this->id]);

            $this->flashMessage(_('Dostupnosť produktu bola úspešne odstránená.'), 'success');
        }

        $this->redirect(':Admin:ShopProductsAvailability:', ['id' => null]);
    }


    /** render ********************************************************************************************************/

    public function renderCreate()
    {
        $this->template->siteTitle = _('Pridať pravidlo');

        $this->template->setFile($this->getTemplatePath() . 'default.latte');
    }


    public function renderEdit()
    {
        $this->template->siteTitle = _('Upraviť pravidlo');
        $this->template->entity = $this->entity;

        $this->template->setFile($this->getTemplatePath() . 'default.latte');
    }


    public function renderDelete()
    {
        $this->template->siteTitle = _('Odstrániť pravidlo');

        $this->template->setFile($this->getTemplatePath() . 'delete.latte');
    }


    /** other *********************************************************************************************************/

    /**
     * Get entity
     *
     * @param bool $check
     *
     * @return ActiveRow
     *
     * @throws \Nette\Application\AbortException
     */
    private function getEntity($check = true)
    {
        if ($this->entity) return $this->entity;

        if (!$this->id && $check === true) {
            $this->flashMessage(_('Nemáte zadané ID pravidla'), 'danger');
            $this->redirect(':Admin:ShopProductsAvailability:', ['id' => null]);
        }

        $this->entity = $this->repository->getRule($this->id);

        if (!$this->entity) {
            $this->flashMessage(_('Pravidlo s takýmto ID sa nenašlo'), 'danger');
            $this->redirect(':Admin:ShopProductsAvailability:', ['id' => null]);
        }

        return $this->entity;
    }


    /**
     * Get template path
     *
     * @return string
     */
    private function getTemplatePath()
    {
        return __DIR__ . '/templates/ShopProductAvailabilityRule/';
    }

}
