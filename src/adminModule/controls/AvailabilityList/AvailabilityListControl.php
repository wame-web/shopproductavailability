<?php

namespace WameCms\ShopProductAvailability\AdminModule\Controls;

use Nette\Application\UI\Control;
use Nette\Database\Table\ActiveRow;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;


interface AvailabilityListControlFactory
{
    /** @return AvailabilityListControl */
    public function create($availability);
}


class AvailabilityListControl extends Control
{
    /** @var ActiveRow|null */
    private $availability;

    /** @var ShopProductAvailabilityRepository */
    private $shopProductAvailabilityRepository;


    public function __construct($availability, ShopProductAvailabilityRepository $shopProductAvailabilityRepository)
    {
        parent::__construct();

        $this->availability = $availability;
        $this->shopProductAvailabilityRepository = $shopProductAvailabilityRepository;
    }


    public function render()
    {
        $this->template->active = $this->availability;
        $this->template->list = $this->shopProductAvailabilityRepository->getAvailabilityList();

        $this->template->setFile(__DIR__ . '/default.latte');
        $this->template->render();
    }

}
