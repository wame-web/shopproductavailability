<?php

namespace WameCms\ShopProductAvailability\AdminModule\Grids;

use Grido\Grid;
use Nette\Application\UI\Control;
use Nette\Utils\Html;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;


interface ShopProductsAvailabilityGridFactory
{
    /** @return ShopProductsAvailabilityGrid */
    public function create();
}


class ShopProductsAvailabilityGrid extends Control
{
    /** @var ShopProductAvailabilityRepository */
	private $repository;

	/** @var string */
	private $lang;


	public function __construct(
	    ShopProductAvailabilityRepository $shopProductAvailabilityRepository
    ) {
		parent::__construct();

		$this->repository = $shopProductAvailabilityRepository;
	}


	public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }


	public function render()
    {
		$this->template->setFile(ABSOLUTE_PATH . '/../app/adminModule/components/grids/Grid.latte');
		$this->template->render();
	}


	protected function createComponentGrid($name)
    {
		$grid = new Grid($this, $name);

		$query = $this->repository->getAvailabilityList($this->getPresenter()->lang);

		$grid->setModel($query);

		$grid->setDefaultSort(['sort' => 'ASC']);
		$grid->setTemplateFile(ABSOLUTE_PATH . '/../app/adminModule/components/grids/admin.latte');
		$grid->setFilterRenderType('inner');
		$grid->getTablePrototype()->class = 'table table-hover borderless';

		// COLUMNS		
		$grid->addColumnText('id', _('Id'))
				->setSortable()
				->getCellPrototype()->addAttributes(['class' => 'bg-primary']);

		$grid->addColumnText('title', _('Názov'))
				->setSortable();

		$grid->addColumnText('color', _('Farba'))
				->setSortable()
				->setCustomRender(function($row) {
					return Html::el('span', ['class' => 'label', 'style' => 'color: #eee; background-color: ' . $row->color])->setText($row->color);
				});

		$grid->addColumnText('icon', _('Ikona'))
				->setSortable();

        $grid->addColumnText('delivery_days', _('Doba dodania v dňoch'))
                ->setSortable();

        $grid->addColumnText('add_to_cart', _('Je možné pridať do košíka'))
                ->setSortable()
                ->setCustomRender(function($row) {
                    return $row->add_to_cart == true ? _('Áno') : _('Nie');
                });

        $grid->addColumnText('sort', _('Poradie'))
                ->setSortable();

		// BUTTONS for actions
		$grid->addActionHref('detail', _('Upraviť'), ':Admin:ShopProductAvailabilityRule:edit')
				->getElementPrototype()
				->setTitle(_('Upraviť'))
				->setClass('btn btn-xs btn-link text-info tooltipTop')
				->setText(Html::el('span')->setClass('glyphicon glyphicon-edit text-info'));

		$grid->addActionHref('delete', _('Odstrániť'), ':Admin:ShopProductAvailabilityRule:delete')
                ->getElementPrototype()
                ->setTitle(_('Odstrániť'))
                ->setClass('btn btn-xs btn-link text-danger ajax-modal tooltipTop')
                ->setText(Html::el('span')->setClass('glyphicon glyphicon-remove text-danger'));
		
		return $grid;
	}

}
