<?php

namespace WameCms\ShopProductAvailability\AdminModule\Grids;

use App\Model\Shop\Store\ShopStoreRepository;
use Grido\Grid;
use Nette\Application\UI\Control;
use Nette\Utils\Html;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRuleConditionRepository;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRuleRepository;


interface ShopProductAvailabilityRuleGridFactory
{
    /** @return ShopProductAvailabilityRuleGrid */
    public function create();
}


class ShopProductAvailabilityRuleGrid extends Control
{
    /** @var ShopProductAvailabilityRuleRepository */
	private $repository;

	/** @var array */
	private $availabilityList;

	/** @var array */
	private $storeList;

	/** @var string */
	private $lang;


	public function __construct(
	    ShopProductAvailabilityRepository $shopProductAvailabilityRepository,
        ShopProductAvailabilityRuleRepository $shopProductAvailabilityRuleRepository,
        ShopStoreRepository $shopStoreRepository
    ) {
		parent::__construct();

		$this->repository = $shopProductAvailabilityRuleRepository;

		$this->availabilityList = $shopProductAvailabilityRepository->getAvailabilityList();
		$this->storeList = $shopStoreRepository->getPairs([], 'id', 'title');
	}


	public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }


	public function render()
    {
		$this->template->setFile(ABSOLUTE_PATH . '/../app/adminModule/components/grids/Grid.latte');
		$this->template->render();
	}


	protected function createComponentGrid($name)
    {
        $self = $this;

		$grid = new Grid($this, $name);

		$query = $this->repository->getRules($this->getPresenter()->id, 'none');

		$grid->setModel($query);

		$grid->setDefaultSort(['sort' => 'ASC']);
		$grid->setTemplateFile(ABSOLUTE_PATH . '/../app/adminModule/components/grids/admin.latte');
		$grid->setFilterRenderType('inner');
		$grid->getTablePrototype()->class = 'table table-hover borderless';

		// COLUMNS		
		$grid->addColumnText('id', _('Id'))
				->setSortable()
				->getCellPrototype()->addAttributes(['class' => 'bg-primary']);

		$grid->addColumnText('availability_id', _('Dostupnosť'))
                ->setSortable()
				->setCustomRender(function($item) use ($self) {
				    if (isset($self->availabilityList[$item['availability_id']])) {
				        $availability = $self->availabilityList[$item['availability_id']];

                        return Html::el('span')->addClass('label')->setStyle(['background-color' => $availability['color']])->setHtml('&nbsp;&nbsp;')
                            . Html::el('strong')->setStyle(['margin-left' => '5px'])->setText($availability['title']);
                    } else {
				        return '-';
                    }
				});


        $grid->addColumnText('conditions', _('Pravidlá'))
                ->setCustomRender(function($item) use ($self) {
                    if (isset($item['conditions']) && count($item['conditions']) > 0) {
                        $return = '';

                        ksort($item['conditions']);

                        foreach ($item['conditions'] as $index => $condition) {
                            $return .= Html::el('small')->setHtml(($index == 0 ? 'WHERE' : $condition['condition']) . ' ' . (isset($self->storeList[$condition['store_id']]) ? $self->storeList[$condition['store_id']] : 'Neexistujúci sklad') . ' ' . $condition['expression'] . ' ' . $condition['value']);
                            if ($condition['note'] == ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_HAVE) {
                                $return .= Html::el('small')->addAttributes(['class' => 'glyphicon glyphicon-edit', 'style' => 'margin-left: 5px; color: orange;']);
                            } elseif ($condition['note'] == ShopProductAvailabilityRuleConditionRepository::NOTE_MUST_NOT_HAVE) {
                                $return .= Html::el('small')->addAttributes(['class' => 'glyphicon glyphicon-edit', 'style' => 'margin-left: 5px; color: red;']);
                            }
                            $return .= Html::el('br');
                        }

                        return $return;
                    } else {
                        return Html::el('span')->addClass('text-muted')->setText(_('Niesú pridané žiadné pravidlá'));
                    }
                });

        $grid->addColumnText('categories', _('Kategórie'))
                ->setCustomRender(function($item) use ($self) {
                    if (isset($item['conditions']) && count($item['conditions']) > 0) {
                        return count($item['categories']);
                    } else {
                        return Html::el('span')->addClass('text-muted')->setText(_('Niesú pridané žiadné kategórie'));
                    }
                });

        $grid->addColumnText('sort', _('Poradie'))
                ->setSortable();

		// BUTTONS for actions
		$grid->addActionHref('detail', _('Upraviť'), ':Admin:ShopProductAvailabilityRule:edit')
				->getElementPrototype()
				->setTitle(_('Upraviť'))
				->setClass('btn btn-xs btn-link text-info tooltipTop')
				->setText(Html::el('span')->setClass('glyphicon glyphicon-edit text-info'));

		$grid->addActionHref('delete', _('Odstrániť'), ':Admin:ShopProductAvailabilityRule:delete')
                ->getElementPrototype()
                ->setTitle(_('Odstrániť'))
                ->setClass('btn btn-xs btn-link text-danger ajax-modal tooltipTop')
                ->setText(Html::el('span')->setClass('glyphicon glyphicon-remove text-danger'));
		
		return $grid;
	}

}
