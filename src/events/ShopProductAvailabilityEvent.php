<?php

namespace WameCms\ShopProductAvailability\Events;

use App\Model\Shop\ShopOrderItemRepository;
//use App\Model\Shop\ShopProductRepository;
use App\Model\Shop\ShopOrderRepository;
//use App\Model\ShopModel;
use Nette\SmartObject;
use WameCms\ShopProductAvailability\Models\MatchingAvailability;
use WameCms\ShopProductAvailability\Models\Regenerate;

//use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;


class ShopProductAvailabilityEvent
{
    use SmartObject;


    /** @var ShopOrderItemRepository */
    private $shopOrderItemRepository;

    /** @var MatchingAvailability */
    private $matchingAvailability;

    /** @var Regenerate */
    private $regenerate;

//    /** @var ShopProductAvailabilityRepository */
//    private $productAvailabilityRepository;


    public function __construct(
//        ShopModel $shopModel,
        ShopOrderRepository $shopOrderRepository,
        ShopOrderItemRepository $shopOrderItemRepository,
        MatchingAvailability $matchingAvailability,
        Regenerate $regenerate
//        ShopProductRepository $shopProductRepository,
//        ShopProductAvailabilityRepository $productAvailabilityRepository
    ) {
        $this->shopOrderItemRepository = $shopOrderItemRepository;
        $this->matchingAvailability = $matchingAvailability;
        $this->regenerate = $regenerate;
//        $this->shopProductRepository = $shopProductRepository;
//        $this->productAvailabilityRepository = $productAvailabilityRepository;

        $shopOrderRepository->onCreateOrder[] = [$this, 'onCreateOrder'];
    }


    public function onCreateOrder($order, $userOrderInfo)
    {
        $this->regenerate->process();

//        $availabilityLevels = $this->productAvailabilityRepository->findBy(['status <> ?' => 0, 'stock_status_change' => 1, 'lang' => $this->shopModel->lang])->fetchPairs('stock_status', 'id');

//        $items = $this->shopOrderItemRepository->findBy(['shop_order_id' => $order->id]);
//
//        foreach ($items as $item) {
//            $this->matchingAvailability->change($item['product_id']);
//        }

//            $product = $this->shopProductRepository->findOneBy(['product_id' => $item->product_id, 'lang' => $this->shopModel->lang]);
//
//            $quantity = $product->stock_status - $item->quantity;
//
//            //je nejaka zmena dostupnosti?
//            $smallestLevel = null;
//            $smallestLevelId = null;
//
//            foreach ($availabilityLevels as $level => $levelId) {
//                if ($quantity <= $level) {
//                    if ($smallestLevel == null || $level <= $smallestLevel) {
//                        $smallestLevel = $level;
//                        $smallestLevelId = $levelId;
//                    }
//                }
//            }
//
//            if ($smallestLevelId != null) {
//                //Ak sme dosialhli niektoru z urovni
//                foreach ($this->shopModel->availableLangs as $lang) {
//                    $smallestLevelLangId = $this->productAvailabilityRepository->findBy(['availability_id' => $smallestLevelId, 'lang' => $lang])->limit(1)->fetch();
//                    $this->shopProductRepository->update(['product_id' => $item->product_id, 'lang' => $lang], ['stock_status' => $quantity, 'availability_id' => $smallestLevelLangId['id']]);
//                }
//            } else {
//                // alebo ulozenie noveho poctu produkto
//                $this->shopProductRepository->update(['product_id' => $item->product_id], ['stock_status' => $quantity]);
//            }
    }

}
