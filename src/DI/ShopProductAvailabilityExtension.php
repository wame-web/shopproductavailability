<?php

namespace WameCms\ShopProductAvailability\DI;

use Nette\DI\CompilerExtension;


class ShopProductAvailabilityExtension extends CompilerExtension
{
    private $defaults = [];


    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();

        // Nette 2.3
//        $this->config += $this->defaults;
//
//        $this->compiler->parseServices($builder, $this->loadFromFile(__DIR__ . '/../config/config.neon'), $this->name);

        // Nette 2.4
        $this->config = $this->validateConfig($this->defaults);

        $this->compiler->loadDefinitions($builder, $this->loadFromFile(__DIR__ . '/../config/config.neon')['services'], $this->name);
    }


    public function beforeCompile()
    {
        parent::beforeCompile();

        $builder = $this->getContainerBuilder();

//        $builder->getDefinition($builder->getByType(ImportUpdateOrderStatus::class))->addSetup('setOptions', [$this->config]);
//
//        $builder->getDefinition($builder->getByType(DeliveryMethodRegister::class))->addSetup('add', [$builder->getDefinition($builder->getByType(DhlDeliveryMethodFactory::class)), 'dhl']);
    }

}