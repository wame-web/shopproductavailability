<?php

namespace WameCms\ShopProductAvailability\Controls;

use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\Database\Row;
use Nette\Database\Table\ActiveRow;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;
use WameCms\Utils\TemplateFinder;


trait ShopProductAvailabilityTrait
{
    /** @var ShopProductAvailabilityControlFactory @inject */
    public $shopProductAvailabilityControlFactory;

    /** @var ShopProductAvailabilityRepository @inject */
    public $shopProductAvailabilityRepository;

    protected function createComponentShopProductAvailability()
    {
        $componentsSettings = $this->componentsSettings['shopProductAvailability'];
        $availabilityList = $this->shopProductAvailabilityRepository->getAvailabilityList($this->lang);

        return new Multiplier(function ($arg) use ($componentsSettings, $availabilityList) {
            list ($position, $id) = explode('_', $arg);

            return $this->shopProductAvailabilityControlFactory->create($componentsSettings[$position], $availabilityList);
        });
    }
}


interface ShopProductAvailabilityControlFactory
{
    /** @return ShopProductAvailabilityControl */
    public function create(array $componentSettings, $availabilityList);
}


class ShopProductAvailabilityControl extends Control
{
    /** @var array */
    private $componentSettings;

    /** @var array */
    private $availabilityList = [];


    public function __construct(
        array $componentSettings,
        $availabilityList
    ) {
        parent::__construct();

        $this->componentSettings = $componentSettings;
        $this->availabilityList = $availabilityList;
    }


    public function render($availabilityId)
    {
        $positionSettings = $this->componentSettings;

        if ($availabilityId instanceof Row) $availabilityId = $availabilityId->id;

        $this->template->availability = !empty($this->availabilityList) && isset($this->availabilityList[$availabilityId]) ? $this->availabilityList[$availabilityId] : null;

        $path = TemplateFinder::getRelativePath($this->getReflection()->getFileName());
        $paths = TemplateFinder::getList($path, ($positionSettings['template'] ?: 'default.latte'));
        $this->template->setFile(TemplateFinder::getFirstExists($paths));

        $this->template->render();
    }

}
