<?php

namespace WameCms\ShopProductAvailability\Controls;

use Nette\DI\Container;
use WameCms\Component\Controls\BaseControl;
use WameCms\ShopProductAvailability\Models\DeliveryDate;
use WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository;


interface DeliveryDateControlFactory
{
    /** @return DeliveryDateControl */
    public function create();
}


class DeliveryDateControl extends BaseControl
{
    /** @var DeliveryDate */
    private $deliveryDate;

    /** @var ShopProductAvailabilityRepository */
    private $shopProductAvailabilityRepository;


    public function __construct(
        Container $container,
        DeliveryDate $deliveryDate,
        ShopProductAvailabilityRepository $shopProductAvailabilityRepository
    ) {
        parent::__construct($container);

        $this->deliveryDate = $deliveryDate;
        $this->shopProductAvailabilityRepository = $shopProductAvailabilityRepository;
    }


    protected function renderDefault(): void
    {
        $availabilityId = $this->getRenderOption('availability!');

        $this->template->availability = $this->shopProductAvailabilityRepository->getAvailability($availabilityId);
        $this->template->text = $this->deliveryDate->getDate($availabilityId);
    }

}
