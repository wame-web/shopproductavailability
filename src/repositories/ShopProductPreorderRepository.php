<?php

namespace WameCms\ShopProductAvailability\Repositories;

use App\Model\BaseRepository;


class ShopProductPreorderRepository extends BaseRepository
{
    public $tableName = 'shop_product_preorder';

}
