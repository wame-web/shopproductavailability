<?php

namespace WameCms\ShopProductAvailability\Repositories;

use App\Model\BaseRepository;


class ShopProductAvailabilityLangRepository extends BaseRepository
{
    /** @var string */
    public $tableName = 'shop_product_availability_lang';

}
