<?php

namespace WameCms\ShopProductAvailability\Repositories;

use App\Model\BaseRepository;


class ShopProductAvailabilityRuleCategoryRepository extends BaseRepository
{
    /** @var string */
    public $tableName = 'shop_product_availability_rule_category';


    /**
     * Create rule categories
     *
     * @param int $ruleId
     * @param array $categories
     *
     * @throws \Exception
     */
    public function create($ruleId, $categories)
    {
        if (count($categories) == 0) return;

        $data = $this->prepareData($ruleId, $categories);

        return $this->insertOrUpdateMultiple($data);
    }


    /**
     * Edit rule categories
     *
     * @param int $ruleId
     * @param array $categories
     *
     * @throws \Exception
     */
    public function edit($ruleId, $categories)
    {
        if (count($categories) == 0) {
            return $this->delete(['rule_id' => $ruleId]);
        }

        $data = $this->prepareData($ruleId, $categories);

        $this->delete(['rule_id' => $ruleId, 'category_id NOT IN (?)' => array_column($data, 'category_id')]);

        return $this->insertOrUpdateMultiple($data);
    }


    /**
     * Get rules categories
     *
     * @param array|int $ruleIds
     *
     * @return array
     */
    public function getCategories($ruleIds)
    {
        $categories = $this->findBy(['rule_id IN (?)' => is_array($ruleIds) ? $ruleIds : [$ruleIds]]);

        $return = [];

        foreach ($categories as $category) {
            if (is_array($ruleIds)) {
                $return[$category['rule_id']][$category['category_id']] = $category['category_id'];
            } else {
                $return[$category['category_id']] = $category['category_id'];
            }
        }

        ksort($return);

        return $return;
    }


    /**
     * Prepare data
     *
     * @param int $ruleId
     * @param array $categories
     *
     * @return array
     */
    private function prepareData($ruleId, $categories)
    {
        $return = [];

        foreach ($categories as $categoryId) {
            $return[] = [
                'rule_id' => $ruleId,
                'category_id' => $categoryId
            ];
        }

        return $return;
    }

}
