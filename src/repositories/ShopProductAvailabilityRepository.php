<?php

namespace WameCms\ShopProductAvailability\Repositories;

use App\Model\BaseRepository;
use h4kuna\Gettext\GettextSetup;
use Nette;


class ShopProductAvailabilityRepository extends BaseRepository
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;


    /** @var string */
    public $tableName = 'shop_product_availability';

    /** @var ShopProductAvailabilityLangRepository */
    public $langRepository;

    /** @var string */
    public $langTableName;


    public function __construct(
        Nette\Database\Context $db,
        Nette\DI\Container $container,
        GettextSetup $translator,
        Nette\Security\User $user,
        ShopProductAvailabilityLangRepository $langRepository
    ) {
        parent::__construct($db, $container, $translator, $user);

        $this->langRepository = $langRepository;
        $this->langTableName = $langRepository->getTableName();
    }


    /**
     * Create availability
     *
     * @param string $lang
     * @param array $values
     *
     * @return \App\Model\IRow|bool|int
     */
    public function create($lang, $values)
    {
        $data = [
            'delivery_days' => $values['delivery_days'],
            'delivery_days_heureka' => $values['delivery_days_heureka'],
            'add_to_cart' => $values['add_to_cart'] == true ? 1 : 0,
            'color' => $values['color'],
            'icon' => $values['icon'],
            'sort' => $values['sort'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_user_id' => $this->user->id,
            'edit_date' => date('Y-m-d H:i:s'),
            'edit_user_id' => $this->user->id
        ];

        $insert = $this->insert($data);

        $langData = [
            'availability_id' => $insert->id,
            'lang' => $lang,
            'title' => $values['title'],
            'description' => $values['description'],
        ];

        $this->langRepository->insert($langData);

        return $insert;
    }


    /**
     * Edit availability
     *
     * @param string $lang
     * @param array $values
     *
     * @return \App\Model\IRow|bool|int
     */
    public function edit($availabilityId, $lang, $values)
    {
        $data = [
            'delivery_days' => $values['delivery_days'],
            'delivery_days_heureka' => $values['delivery_days_heureka'],
            'add_to_cart' => $values['add_to_cart'] == true ? 1 : 0,
            'color' => $values['color'],
            'icon' => $values['icon'],
            'sort' => $values['sort'],
            'edit_date' => date('Y-m-d H:i:s'),
            'edit_user_id' => $this->user->id
        ];

        $this->update(['id' => $availabilityId], $data);

        $langData = [
            'title' => $values['title'],
            'description' => $values['description'],
        ];

        $this->langRepository->update(['availability_id' => $availabilityId, 'lang' => $lang], $langData);

        return $availabilityId;
    }


    /**
     * Get availability by ID
     *
     * @param int $availabilityId
     *
     * @return bool|Nette\Database\IRow|Nette\Database\Row
     */
    public function getAvailability($availabilityId)
    {
        $query = "SELECT a.*, l.title, l.description "
            . "FROM " . $this->getTableName() . " AS a "
            . "LEFT JOIN " . $this->langTableName . " AS l ON l.availability_id = a.id "
            . "WHERE l.lang = ? AND a.id = ?";

        return $this->db->query($query, $this->lang, $availabilityId)->fetch();
    }


    /**
     * Get availability list
     *
     * @param string|null $lang
     * @param array|null $where
     *
     * @return array
     */
    public function getAvailabilityList($lang = null, $where = null)
    {
        if (!$lang) $lang = $this->lang;

        $query = "SELECT a.*, l.title, l.description "
            . "FROM " . $this->getTableName() . " AS a "
            . "LEFT JOIN " . $this->langTableName . " AS l ON l.availability_id = a.id "
            . "WHERE l.lang = ? AND a.status = ? ";

        $args = [$lang, self::STATUS_ACTIVE];

        if ($where) {
            foreach ($where as $key => $value) {
                $query .= $key;
                $args[] = $value;
            }
        }

        $query .= " ORDER BY a.sort ASC";

        return $this->db->queryArgs($query, $args)->fetchPairs('id');
    }


    /**
     * Get availability select pairs
     *
     * @return array
     */
    public function getAvailabilitiesSelect()
    {
        $query = "SELECT a.id, l.title "
                . "FROM " . $this->getTableName() . " AS a "
                . "LEFT JOIN " . $this->langTableName . " AS l ON l.availability_id = a.id "
                . "WHERE l.lang = ? AND a.status = ? "
                . "ORDER BY a.sort ASC";

        return $this->db->query($query, $this->lang, self::STATUS_ACTIVE)->fetchPairs('id', 'title');
    }


    /**
     * Get columns alias
     *
     * @param string $prefix
     *
     * @return array
     */
    public function getColumns($prefix = '')
    {
        $langPrefix = substr($prefix, 0, -1) . '_lang.';

        return [
            $langPrefix . 'title AS availabilityTitle',
            $langPrefix . 'description AS availabilityDescription',
            $prefix . 'color AS availabilityColor',
            $prefix . 'icon AS availabilityIcon',
        ];
    }

}
