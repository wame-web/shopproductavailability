<?php

namespace WameCms\ShopProductAvailability\Repositories;

use App\Model\BaseRepository;
use h4kuna\Gettext\GettextSetup;
use Nette;


class ShopProductAvailabilityRuleConditionRepository extends BaseRepository
{
    const NOTE_NONE = '0';
    const NOTE_MUST_HAVE = '1';
    const NOTE_MUST_NOT_HAVE = '2';


    /** @var string */
    public $tableName = 'shop_product_availability_rule_condition';


    /**
     * Create availability rules
     *
     * @param int $ruleId
     * @param array $conditions
     *
     * @throws \Exception
     */
    public function create($ruleId, $conditions)
    {
        if (count($conditions) == 0) return;

        foreach ($this->prepareData($ruleId, $conditions) as $condition) {
            $this->insert($condition);
        }
    }


    /**
     * Update availability rules
     *
     * @param int $ruleId
     * @param array $conditions
     *
     * @return int|void
     *
     * @throws \Exception
     */
    public function edit($ruleId, $conditions)
    {
        $list = $this->getObjectPairs(['rule_id' => $ruleId], 'sort', 'sort ASC');
        $newCount = count($conditions);

        if (count($list) > $newCount) {
            $this->delete(['rule_id' => $ruleId, 'sort > ?' => $newCount - 1]);
        }

        $i = 0;

        foreach ($this->prepareData($ruleId, $conditions) as $sort => $condition) {
            if ($i <= $newCount - 1) {
                if (isset($list[$i])) {
                    $this->update(['id' => $list[$i]['id']], $condition);
                } else {
                    $insert = $this->insert($condition);
                }
            } else {
                $insert = $this->insert($condition);
            }

            $i++;
        }
    }


    /**
     * Get conditions
     *
     * @param array|int $ruleId
     *
     * @return array
     */
    public function getConditions($ruleIds)
    {
        $conditions = $this->findBy(['rule_id IN (?)' => is_array($ruleIds) ? $ruleIds : [$ruleIds]], 'sort ASC');

        $return = [];

        foreach ($conditions as $condition) {
            $data = [
                'condition' => $condition['condition'] == 'WHERE' ? 'AND' : $condition['condition'],
                'store_id' => $condition['store_id'],
                'expression' => $condition['expression'],
                'value' => $condition['value'],
                'note' => $condition['note'],
                'sort' => $condition['sort']
            ];

            if (is_array($ruleIds)) {
                $return[$condition['rule_id']][$condition['sort']] = $data;
            } else {
                $return[$condition['sort']] = $data;
            }
        }

        ksort($return);

        return $return;
    }


    /**
     * Prepare data
     *
     * @param int $ruleId
     * @param array $conditions
     *
     * @return array
     */
    private function prepareData($ruleId, $conditions)
    {
        $return = [];

        foreach ($conditions as $sort => $condition) {
            $return[$sort] = [
                'rule_id' => $ruleId,
                'condition' => $sort == 0 ? 'WHERE' : $condition['condition'],
                'store_id' => $condition['store_id'],
                'expression' => $condition['expression'],
                'value' => $condition['value'],
                'note' => strval($condition['note']),
                'sort' => $sort
            ];
        }

        return $return;
    }

}
