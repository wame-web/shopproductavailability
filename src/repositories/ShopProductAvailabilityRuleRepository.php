<?php

namespace WameCms\ShopProductAvailability\Repositories;

use App\Model\BaseRepository;
use h4kuna\Gettext\GettextSetup;
use Nette;


class ShopProductAvailabilityRuleRepository extends BaseRepository
{
    /** @var string */
    public $tableName = 'shop_product_availability_rule';

    /** @var ShopProductAvailabilityRuleCategoryRepository */
    private $shopProductAvailabilityRuleCategoryRepository;

    /** @var ShopProductAvailabilityRuleConditionRepository */
    private $shopProductAvailabilityRuleConditionRepository;


    public function __construct(
        Nette\Database\Context $db,
        Nette\DI\Container $container,
        GettextSetup $translator,
        Nette\Security\User $user,
        ShopProductAvailabilityRuleCategoryRepository $shopProductAvailabilityRuleCategoryRepository,
        ShopProductAvailabilityRuleConditionRepository $shopProductAvailabilityRuleConditionRepository
    ) {
        parent::__construct($db, $container, $translator, $user);

        $this->shopProductAvailabilityRuleCategoryRepository = $shopProductAvailabilityRuleCategoryRepository;
        $this->shopProductAvailabilityRuleConditionRepository = $shopProductAvailabilityRuleConditionRepository;
    }


    /**
     * Create availability rules
     *
     * @param array $values
     *
     * @throws \Exception
     */
    public function create($values)
    {
        $data = [
            'availability_id' => $values['availability_id'],
            'sort' => $values['sort']
        ];

        

        $insert = $this->insert($data);

        $ruleId = $insert->id;

        $this->shopProductAvailabilityRuleCategoryRepository->create($ruleId, $values['categories']);
        $this->shopProductAvailabilityRuleConditionRepository->create($ruleId, $values['conditions']);

        return $insert;
    }


    /**
     * Update availability rules
     *
     * @param array $availabilityId
     * @param array $rules
     *
     * @return int|void
     *
     * @throws \Exception
     */
    public function edit($ruleId, $values)
    {
        $data = [
            'availability_id' => $values['availability_id'],
            'sort' => $values['sort']
        ];

        $this->update(['id' => $ruleId], $data);

        $this->shopProductAvailabilityRuleCategoryRepository->edit($ruleId, $values['categories']);
        $this->shopProductAvailabilityRuleConditionRepository->edit($ruleId, $values['conditions']);
    }


    /**
     * Get rule
     *
     * @param int $ruleId
     *
     * @return array
     */
    public function getRule($ruleId)
    {
        $rule = $this->findOneBy(['id' => $ruleId]);

        $return = $rule->toArray();
        $return['categories'] = $this->shopProductAvailabilityRuleCategoryRepository->getCategories($ruleId);
        $return['conditions'] = $this->shopProductAvailabilityRuleConditionRepository->getConditions($ruleId);

        return $return;
    }


    /**
     * Get rules by availability id
     *
     * @param int|null $availabilityId
     *
     * @return array
     */
    public function getRules($availabilityId = null, $multi = true)
    {
        $where = [];

        if ($availabilityId) $where['availability_id'] = $availabilityId;

        $list = $this->getObjectPairs($where, 'id');

        $ruleIds = array_keys($list);

        $categories = $this->shopProductAvailabilityRuleCategoryRepository->getCategories($ruleIds);
        $conditions = $this->shopProductAvailabilityRuleConditionRepository->getConditions($ruleIds);

        $return = [];

        foreach ($list as $item) {
            $itemId = $item['id'];

            $data = $item->toArray();
            $data['categories'] = isset($categories[$itemId]) ? $categories[$itemId] : [];
            $data['conditions'] = isset($conditions[$itemId]) ? $conditions[$itemId] : [];

            if ($multi === true) {
                $return[$item['availability_id']][] = $data;
            } elseif ($multi == 'none') {
                $return[] = $data;
            } else {
                $return[$item['sort']] = $data;
            }
        }

        ksort($return);

        return $return;
    }

}
