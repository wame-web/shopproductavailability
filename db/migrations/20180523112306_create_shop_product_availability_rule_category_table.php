<?php

use Phinx\Migration\AbstractMigration;


class CreateShopProductAvailabilityRuleCategoryTable extends AbstractMigration
{
    /** {@inheritdoc} */
    public function up()
    {
        $table = $this->table('wame_shop_product_availability_rule_category');
        $table->addColumn('rule_id', 'integer', ['null' => true])
                ->addColumn('category_id', 'integer')
                ->addForeignKey('rule_id', 'wame_shop_product_availability_rule', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                ->addIndex(['rule_id', 'category_id'], ['unique' => true])
                ->create();
    }


    /** {@inheritdoc} */
    public function down()
    {
        $this->dropTable('wame_shop_product_availability_rule_category');
    }

}
