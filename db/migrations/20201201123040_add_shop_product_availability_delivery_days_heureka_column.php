<?php

use Phinx\Migration\AbstractMigration;


class AddShopProductAvailabilityDeliveryDaysHeurekaColumn extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('wame_shop_product_availability');
        $table->addColumn('delivery_days_heureka', 'integer', ['length' => 3, 'after' => 'delivery_days'])
                ->save();
    }

}
