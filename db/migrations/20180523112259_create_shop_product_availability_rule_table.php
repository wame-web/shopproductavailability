<?php

use Phinx\Migration\AbstractMigration;


class CreateShopProductAvailabilityRuleTable extends AbstractMigration
{
    /** {@inheritdoc} */
    public function up()
    {
        $table = $this->table('wame_shop_product_availability_rule');
        $table->addColumn('availability_id', 'integer', ['null' => true, 'length' => 10, 'signed' => false])
                ->addColumn('sort', 'integer', ['length' => 5])
                ->addForeignKey('availability_id', 'wame_shop_product_availability', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                ->create();
    }


    /** {@inheritdoc} */
    public function down()
    {
        $this->dropTable('wame_shop_product_availability_rule');
    }

}
