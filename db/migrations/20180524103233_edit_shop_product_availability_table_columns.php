<?php

use Phinx\Migration\AbstractMigration;


class EditShopProductAvailabilityTableColumns extends AbstractMigration
{
    /** {@inheritdoc} */
    public function up()
    {
        $table = $this->table('wame_shop_product_availability');
        $table->addColumn('add_to_cart', 'boolean', ['default' => true, 'after' => 'delivery_days'])
            ->removeColumn('availability_id')
            ->removeColumn('lang')
            ->removeColumn('title')
            ->removeColumn('stock_status')
            ->removeColumn('stock_status_change')
            ->removeColumn('description')
            ->addForeignKey('create_user_id', 'wame_user', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
            ->addForeignKey('edit_user_id', 'wame_user', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
            ->save();
    }


    /** {@inheritdoc} */
    public function down()
    {
        $table = $this->table('wame_shop_product_availability');
        $table->removeColumn('add_to_cart')
            ->addColumn('availability_id', 'integer', ['length' => 10, 'signed' => false, 'default' => 0, 'after' => 'id'])
            ->addColumn('lang', 'string', ['length' => 2, 'default' => 'sk', 'after' => 'availability_id'])
            ->addColumn('title', 'string', ['length' => 200, 'null' => true, 'after' => 'lang'])
            ->addColumn('description', 'string', ['length' => 65532, 'after' => 'title'])
            ->addColumn('stock_status', 'string', ['length' => 200, 'null' => true, 'after' => 'description'])
            ->addColumn('stock_status_change', 'integer', ['length' => 3, 'signed' => false, 'default' => 0, 'after' => 'stock_status'])
            ->save();
    }

}
