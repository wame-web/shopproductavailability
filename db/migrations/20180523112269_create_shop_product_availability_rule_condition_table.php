<?php

use Phinx\Migration\AbstractMigration;


class CreateShopProductAvailabilityRuleConditionTable extends AbstractMigration
{
    /** {@inheritdoc} */
    public function up()
    {
        $table = $this->table('wame_shop_product_availability_rule_condition');
        $table->addColumn('rule_id', 'integer', ['null' => true])
                ->addColumn('condition', 'string', ['length' => 5, 'null' => true])
                ->addColumn('store_id', 'integer', ['null' => true])
                ->addColumn('expression', 'string', ['length' => 2])
                ->addColumn('value', 'integer')
                ->addColumn('sort', 'integer', ['length' => 4])
                ->addForeignKey('rule_id', 'wame_shop_product_availability_rule', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                ->addForeignKey('store_id', 'wame_shop_store', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                ->create();
    }


    /** {@inheritdoc} */
    public function down()
    {
        $this->dropTable('wame_shop_product_availability_rule_condition');
    }

}
