<?php

use Phinx\Migration\AbstractMigration;


class CreateShopProductAvailabilityLangTable extends AbstractMigration
{
    /** {@inheritdoc} */
    public function up()
    {
        $table = $this->table('wame_shop_product_availability_lang');
        $table->addColumn('availability_id', 'integer', ['null' => true, 'length' => 10, 'signed' => false])
                ->addColumn('lang', 'string', ['length' => 2])
                ->addColumn('title', 'string')
                ->addColumn('description', 'string')
                ->addForeignKey('availability_id', 'wame_shop_product_availability', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                ->create();
    }


    /** {@inheritdoc} */
    public function down()
    {
        $this->dropTable('wame_shop_product_availability_lang');
    }

}
