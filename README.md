# ShopProductAvailability for CMS WAME admin 3

## Inštalácia

1. Do `composer.json` pridať
    ```
    "repositories": [
        {
            "type": "composer",
            "url": "http://packages.wame.sk/",
            "options": {
                "ssh2": {
                    "username": "composer",
                    "pubkey_file": ".ssh/id_rsa.pub",
                    "privkey_file": ".ssh/id_rsa"
                }
            }
        }
	],
	"config": { "secure-http": false },
	"require": {
	    "wamecms/shopProductAvailability": "dev-master"
	}
    ```
2. Zálohovať tabuľku `wame_shop_product_availability`
3. Z priečinka `vendor/wamecms/shopProductAvailability/db/migrations` skopírovať migrácie do `db/migrations` a spustiť ich
4. Skopírovať názvy a popisy dostupnosti ktoré boli v pôvodnej tabuľke `wame_shop_product_availability` do `wame_shop_product_availability_lang`
5. V administrácii povytvárať pravidlá pre jednotlivé dostupnosti


## Integrácia

### Čo treba odstrániť

1. Odstrániť súbory
    - `app/adminModule/components/grids/ShopProductsAvailability/`
    - `app/adminModule/presenters/ShopProductsAvailabilityPresenter.php`
    - `app/adminModule/templates/ShopProductsAvailability/`
    - `app/components/Shop/ShopProductAvailability`
    - `app/model/Shop/ShopProductAvailabilityRepository.php`
    
2. V `app/config/config.neon` odstrániť riadky
    - `- {class: App\Model\Shop\ShopProductAvailabilityRepositoryEvents, tags: [run]}`
    - `- App\AdminModule\Components\Grids\ShopProductAvailability\ShopProductsAvailabilityGridFactory`
    - `- App\Model\Shop\ShopProductAvailabilityRepository`
    - `- App\Components\Shop\ShopProductAvailabilityControlFactory`

3. V `app/adminModule/components/LoadFactories.php` odstrániť funkciu
    - `createComponentShopProductsAvailabilityGrid()`

4. V `app/components/LoadFactories.php` odstrániť trait
    - `use App\Components\Shop\ShopProductAvailability;`
    
5. Vyhľadať všade `\App\Model\Shop\ShopProductAvailabilityRepository` 
    a nahradiť ho `WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository`


### Čo treba pridať/upraviť

1. V `app/bootstrap.php` pridať cestu ak tam už nieje
    ```
    $configurator->createRobotLoader()
                ->addDirectory(__DIR__)
                ->addDirectory(__DIR__ . '/../vendor/wamecms')
                ->register();
    ```
2. V `app/config/config.neon` pridať extension
    ```
    extensions:
        ...
        shopProductAvailability: WameCms\ShopProductAvailability\DI\ShopProductAvailabilityExtension
    ```
3. V `app/model/Shop/ShopProductRepository.php` 
    - upraviť cestu k repository na
    ```
    \WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository
    ```
    - pridať do metódy 
    ```
    public function getProducts($productId, $options = [], $descendants = false)
    {
        ...
        
        if (isset($options['availability']) && $options['availability'] == 1) {
            $availabilityColumns = $this->shopProductAvailabilityRepository->getColumns('availability.');
    
            foreach ($availabilityColumns as $column) {
                $select[] = $column;
            }
    
            $joins[] = [
                'table' => $productAvailabilityTable . ' AS availability',
                'condition' => Conditions::make('availability.id = product.availability_id')
            ];
            
            $joins[] = [
                'table' => $productAvailabilityTable . '_lang AS availability_lang',
                'condition' => Conditions::make('availability.id = availability_lang.availability_id')
            ];
        }
        
        $query = SelectQuery::make(...$select)->from($productTable . ' AS product');
        
        ...
    }    
    ```
4. Do `app/adminModule/presenters/ShopProductsPresenter.php` 
    - injectneme `ShopProductAvailabilityRepository`
    ```
    /** @var ShopProductAvailabilityRepository @inject */
    public $shopProductAvailabilityRepository;
    ```
    - do `renderProduct()` pridáme
    ```
    public function renderProduct($id) 
    {
        ...
        
        $this->template->mainProductAvailability = $this->shopProductAvailabilityRepository->getAvailability($this->productInfo->availability_id);
    }
    ```   
    - do `renderDescendantProduct()` pridáme
    ```
    public function renderDescendantProduct($id, $ofProduct)
    {
        ...
        
        $this->template->mainProductAvailability = $this->shopProductAvailabilityRepository->getAvailability($this->productInfo->availability_id);
    }
    ```
5. V `app/adminModule/templates/ShopProducts/blocks/store.latte`
    ```
    <th class="text-center" style="vertical-align: middle;">
        <div class="label" style="background-color: {$mainProductAvailability->color|noescape}">{$mainProductAvailability->title}</div>
    </th>
    ```
6. V `app/model/Filter/Filterable.php` 
    - upraviť v metódie `getProductsFromCategory()` `availability.availability_id` na `availability.id`
    ```
    ->leftJoin('wame_shop_product_availability availability', Conditions::make('availability.id = product.availability_id'))
    ->leftJoin('wame_shop_product_availability descendant_availability', Conditions::make('descendant_availability.id = descendant_product.availability_id'))
    ```
    - upraviť metódu `getAvailability()`
    ```
    private function getAvailability()
    {
        $conditions = Conditions::make()
            ->andWith('availability.status = ?', 1)
            ->andWith('availability_lang.lang = ?', $this->lang);

        $select = SelectQuery::make('availability_lang.title', 'availability.id')
            ->from('wame_shop_product_availability availability')
            ->leftJoin('wame_shop_product_availability_lang availability_lang', 'availability.id = availability_lang.availability_id')
            ->innerJoin('wame_shop_product product', Conditions::make('product.availability_id = availability.availability_id'));

        $this->addRelationToCategory($select, $conditions);

        $select->where($conditions);

        return $this->database->queryArgs($select->sql(), $select->params())->fetchPairs('availability_id', 'title');
    }
    ```
7. V `app/components/Shop/BestSellingProducts/BestSellingProducts.php` pridať use
    ```
    use WameCms\ShopProductAvailability\Controls\ShopProductAvailabilityControlFactory;
    ```
8. Do `app/presenters/ShopCategoryPresenter.php` pridať trait
    ```
    use \WameCms\ShopProductAvailability\Controls\ShopProductAvailabilityTrait;
    ```
9. Do `app/presenters/ShopOrdersPresenter.php` pridať trait
    ```
    use \WameCms\ShopProductAvailability\Controls\ShopProductAvailabilityTrait;
    ```
10. V `app/model/DeliveryDateCalculator.php` upraviť v metóde `getAvailabilitiesDeliveryDates()`
    ```
    $availabilities = $this->shopProductAvailabilityRepository->getAvailabilityList($this->shopProductAvailabilityRepository->lang);
    
    ...
    
    $deliveryDays[$availability->id] = $this->calculateDeliveryDate($days);
    ```
11. V `app/adminModule/presenters/ShopOrdersPresenter.php` upraviť v metóde `actionDetail()`
    ```
    $tableProductAvailability  = $this->prefix.'shop_product_availability';
    $tableProductAvailabilityLang  = $this->prefix.'shop_product_availability_lang';
    $this->orderItems = $this->database->query("SELECT oi.*, p.credit AS credit, p.stock_status AS stock_status, p.descendant_product AS descendant_product, pa.title AS availability_title, pad.color AS availability_color "
            . "FROM $tableOrderItem AS oi "
            . "LEFT JOIN $tableProduct AS p ON p.product_id = oi.product_id "
            . "LEFT JOIN $tableProductAvailability AS pad ON pad.id = p.availability_id "
            . "LEFT JOIN $tableProductAvailabilityLang AS pa ON pa.availability_id = pad.id "
            . "WHERE oi.shop_order_id = ? AND oi.status <> ? AND p.lang = ? AND pa.lang = ?", $id, '0', $this->lang, $this->lang)->fetchAll();
    ```
12. V `app/model/ShopFilters.php`
    - pridať do constructu
    ```
    WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository
    ```
    -  upraviť metódu `availability()`
    ```
    public function availability(string $availability): string
    {
        $this->availability = $this->availability ?? $this->shopProductAvailabilityRepository->getAvailabilitiesSelect();

        $explode = explode(',', $availability);

        foreach ($this->availability as $type) {
            if (in_array($type['id'], $explode)) {
                return $type['title'];
            }
        }

        return '';
    }
    ```
13. V `app/model/ShopModel.php`
    - pridať do constructu
    ```
    WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository
    ```
    - upraviť v metóde `setProductInfo()` aj pre create a edit
    ```
    $availability = $this->shopProductAvailabilityRepository->getAvailability($avid);
    ```
    - upraviť v metóde `getDescendantProducts()` query
    ```
    $get = $this->db->query("
        SELECT sp.*, spp.price_with_tax as price, spavl.title as availability_title, spav.color as availability_color $attrbituesColumns
        FROM " . $this->prefix . "shop_product AS sp
        LEFT JOIN " . $this->prefix . "shop_product_attribute AS spa ON spa.shop_products_id = sp.product_id
        LEFT JOIN " . $this->prefix . "shop_product_price AS spp ON spp.shop_product_id = sp.product_id
        LEFT JOIN " . $this->prefix . "shop_product_availability AS spav ON spav.id = sp.availability_id
        LEFT JOIN " . $this->prefix . "shop_product_availability_lang AS spavl ON spavl.id = spav.availability_id
        WHERE (sp.lang = ?) AND (spavl.lang = ?) AND (`descendant_product` = ?) AND (sp.status = ?) AND (spp.status = ?)
        ORDER BY `descendant_product_sort` ASC", $this->lang, $this->lang, $productId, 2, 1)->fetchAll();
    ```
14. V `app/model/Heureka/HeurekaAvailabilityFeed.php`
    - vemeniť cestu k repository
    ```
    WameCms\ShopProductAvailability\Repositories\ShopProductAvailabilityRepository
    ```
    - upraviť v metóde `getItems()`
    ```
    $this->availabilityList = $this->shopProductAvailabilityRepository->getAvailabilityList($this->lang, ['AND a.delivery_days <= ?' => 7]);
    ```
    